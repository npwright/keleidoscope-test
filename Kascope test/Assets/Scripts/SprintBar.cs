﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SprintBar : MonoBehaviour
{

    #region Variables

    public GameManager gameManager;
    public Slider slider;
    public Gradient gradient;
    public Image fill;

    #endregion

    #region Main Methods

    void Start(){
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
    
    #endregion

    #region Helper Methods

    // Sets Sprint value for Slider
    public void SetSprint(float sprintValue){
        slider.value = sprintValue;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }

    // Sets Sprint max value for Slider
    public void SetMaxSprint(float maxSprintValue, float sprintValue){
        slider.maxValue = maxSprintValue;
        slider.value = sprintValue;
        fill.color = gradient.Evaluate(1f);
    }
   
   #endregion
}
