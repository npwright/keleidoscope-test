﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotation : MonoBehaviour
{

    #region Variables

    public Vector3 speed;

    #endregion

    #region  Main Methods

    void Update()
    {
        transform.Rotate(speed * Time.deltaTime, Space.World);
    }

    #endregion
}
