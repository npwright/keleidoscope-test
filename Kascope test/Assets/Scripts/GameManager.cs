﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{

    #region Variables

    public int targetsHit;
    public float timer;
    public float startTime = 0f;
    public bool timerPaused = false;

    public bool gameIsPaused = false;
    public bool gameStarted = false;
    public GameObject pauseMenu;
    public GameObject startMenu;
    public GameObject inGameUI;
    public GameObject endScreen;

    public TextMeshProUGUI fullAutoText;

    public TextMeshProUGUI timerText;
    public TextMeshProUGUI targetScore;
    public TextMeshProUGUI finishTime;

    public Color32 pauseColor;
    private Color32 defaultColor;

    public bool fullAuto;
    public float fullAutoTimer = 3f;

    public float sprintValue = 0f;
    public float maxSprintValue = 5f;
    public SprintBar sprintBar;

    #endregion

    #region Main Methods

    void Start(){
        Time.timeScale = 0f;
        Cursor.lockState = CursorLockMode.None;
        startMenu.SetActive(true);
        pauseMenu.SetActive(false);
        inGameUI.SetActive(false);
        sprintBar.SetMaxSprint(maxSprintValue,sprintValue);

        defaultColor = timerText.color;

    }

    void Update(){
        if(Input.GetButtonDown("Pause")){
            Debug.Log("Pause pressed");
            if(gameIsPaused && gameStarted){
                Resume();
            }
            else if(gameIsPaused == false && gameStarted == true){
                Debug.Log("calling Pause()");
                Pause();
            }
        }

        if(gameIsPaused != true && gameStarted == true){
            if(timerPaused != true){
                timer += Time.deltaTime; 
                TimerTextUpdate();
            }
        }

        sprintBar.SetSprint(sprintValue);

    }

    #endregion

    #region Menu Methods
    
    // Exit Menu Button
    public void Exit(){
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }

    // Resume Menu Button
    public void Resume(){
        Time.timeScale = 1f;
        gameIsPaused = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        pauseMenu.SetActive(false);
        inGameUI.SetActive(true);
    }

    // Pause Menu Button
    public void Pause(){
            Time.timeScale = 0f;
            gameIsPaused = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            pauseMenu.SetActive(true);
            inGameUI.SetActive(false);
    }

    // Start Menu Button
    public void StartGame(){
        gameStarted = true;
        Time.timeScale = 1f;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        startMenu.SetActive(false);
        inGameUI.SetActive(true);
    }

    // Restart Menu Button
    public void Restart(){
        SceneManager.LoadScene("SampleScene");
    }

    #endregion

    #region Timer Methods

    // Updates Timer UI Text
    void TimerTextUpdate(){
        string minutes = Mathf.Floor((timer % 3600)/60).ToString("00");
        string seconds = Mathf.Floor(timer % 60).ToString("00");

        timerText.text = minutes + ":" + seconds;
    }

    // IE Numerator for pausing Timer
    public IEnumerator TimerPause(float duration){
        timerText.color = pauseColor;
        timerPaused = true;

        yield return new WaitForSeconds(duration);

        timerPaused = false;
        timerText.color = defaultColor;
    }

    #endregion

    #region End Screen

    // Actions that happen when entering End Zone
    public void EndScreen(){           
        Time.timeScale = 0f;
        gameStarted = false;
        endScreen.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
                
        targetScore.text = targetsHit.ToString("0");

        string minutes = Mathf.Floor((timer % 3600)/60).ToString("00");
        string seconds = (timer % 60).ToString("00");
        finishTime.text = minutes + ":" + seconds;
    }

    #endregion

    #region Power Ups

    // IE Numerator that counts down for Full Auto Powerup
    public IEnumerator CountDown(Collider other, float duration){
        fullAutoText.gameObject.SetActive(true);
        fullAuto = true;

        other.GetComponent<MeshRenderer>().enabled = false;
        other.GetComponent<Collider>().enabled = false;
        yield return new WaitForSeconds (duration);

        fullAuto = false;
        Destroy(other.gameObject);
    }

    // IE Numerator that counts down for Sprint UI slider
    public IEnumerator ChangeValueOerTime(float sprintValue, float maxSprintValue, float duration){
        float counter = 0;

        while (counter < duration){
            if (Time.timeScale == 1f){
                counter += Time.deltaTime;
            }else{
                counter += Time.deltaTime;
            }

            float val = Mathf.Lerp(sprintValue, maxSprintValue, counter / duration);
            yield return null;
        }
    }

    #endregion
}
